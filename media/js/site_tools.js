
/* ------- PAGE LOADER ------- */
window.addEventListener('load', function(){
    var loading_obj = document.getElementById('loader');

    if (loading_obj) {
        loading_obj.style['opacity'] = 0;

        setInterval(function(){
            loading_obj.style['visibility'] = 'hidden';
        }, 1000);
    }
});
/* --------------------------- */


var bgs = [
    { bg : 'media/pics/bgs/bg1.jpg' },
    { bg : 'media/pics/bgs/bg2.jpg' },
    { bg : 'media/pics/bgs/bg3.jpg' },
    { bg : 'media/pics/bgs/bg4.jpg' },
    { bg : 'media/pics/bgs/bg5.jpg' },
];

function getRandomInt(min,max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

$(document).ready(function() {
    var bgNum = getRandomInt(0, bgs.length-1);

    $('body').css('background', 'url('+bgs[bgNum].bg+')'+'no-repeat 0 0 fixed');
    $('body').css('background-size', 'cover');
    

});



/* Window flags. */
var win_data = {
    contacts_flag : false,    /* contacts. */
    order_form_flag : true,
};

function toggleFrame(elem_id) {
    var div_elem = document.getElementById(elem_id);

    if (!win_data.contacts_flag) {
        div_elem.style['visibility'] = 'visible';
        div_elem.style['opacity'] = '1';
    } else {
        div_elem.style['opacity'] = '0';
        setTimeout( function(){ div_elem.style['visibility'] = 'hidden' }, 500);
    }
    win_data.contacts_flag = !win_data.contacts_flag;
}



function scrollWin(direction) {
    var cur_offset = window.pageYOffset;
    var offset = 150;

    if (direction == "up") {
        window.scrollTo(0, cur_offset - offset);
    } else {
        window.scrollTo(0, cur_offset + offset);
    }
}

/* Javascript string format. */
String.prototype.format = function() {
    var formatted = this;
    for( var arg in arguments ) {
        formatted = formatted.replace("{" + arg + "}", arguments[arg]);
    }
    return formatted;
};
/*****************************/


/* Form functions. */

function checkEmail(val) {
    if (val.match(/\S+@\S+\.\S+/)) 
        return false;

    return true;
}


/*

function checkForm() {
    my_form = document.getElementById('order_form');
    my_form_length = my_form.length;

    var input_error = false;
    
    for (var i = 0; i < my_form_length; i++) {

        // If the input is empty make it red.
        if (my_form[i].value.length === 0) {
            my_form[i].style['background'] = '#fcc7c7';
            input_error = true;
        } else {
            my_form[i].style['background'] = '#ffffff';
        }

        // Get email string for check.
        if (my_form[i].type == 'email') {
            if (checkEmail(my_form[i].value) ) {
                my_form[i].style['background'] = '#fcc7c7';
                input_error = true;
            }
        }
    }

    if (!input_error) {

        // Keep user data.
        var user_name = document.getElementsByName('user_name')[0].value;
        var user_postal = document.getElementsByName('user_postal')[0].value;
        var user_text = document.getElementsByName('user_text')[0].value;

        // Clean form.
        var form_div = document.getElementById('order_form_wrap');
        for (var i = 0; i < 4; i++ )    // Remove last 4 nodes.
            form_div.removeChild(form_div.lastChild);
        
        // Create query string.
        var query_string = "sender_name={0}&sender_email={1}&message_text={2}".format(user_name, user_postal, user_text);

        // Make request.
        var req = new XMLHttpRequest();

        req.open('POST', '/send_email');
        req.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        req.send(query_string);     // Set Content-Length from string in send.

        req.onreadystatechange = function () {
            if(req.readyState === 4) {
                var res = req.responseText;

                var response_node = document.createElement('p');
                response_node.style['position'] = 'relative';
                response_node.style['top'] = '50%';
                response_node.style['textAlign'] = 'center';

                if (res === 'OK') {
                    var response_text = document.createTextNode('Спасибо, в ближайшее время мы с Вами свяжемся.');
                    response_node.appendChild(response_text);

                    form_div.appendChild(response_node);
                    
                } else {
                    var response_text = document.createTextNode('Не отправлено, где-то ошибка.');
                    response_node.appendChild(response_text);
                    form_div.appendChild(response_node);
                }
            } 

        }

        setTimeout(function() {
            toggleFrame("order_form_wrap")
        }, 4000);
    }
}
*/



function checkForm(form_name) {

    my_form = document.getElementById(form_name);
    my_form_length = my_form.length;

    var input_error = false;
    
    for (var i = 0; i < my_form_length; i++) {

        // If the input is empty make it red.
        if (my_form[i].value.length === 0) {
            my_form[i].style['background'] = '#fcc7c7';
            input_error = true;
        } else {
            my_form[i].style['background'] = '#ffffff';
        }

        // Get email string for check.
        if (my_form[i].type == 'email') {
            if (checkEmail(my_form[i].value) ) {
                my_form[i].style['background'] = '#fcc7c7';
                input_error = true;
            }
        }
    }

    return input_error;
}


function send_email() {

    if (!checkForm('order_form')) {

        // Keep user data.
        var user_name = document.getElementsByName('user_name')[0].value;
        var user_postal = document.getElementsByName('user_postal')[0].value;
        var user_text = document.getElementsByName('user_text')[0].value;

        // Clean form.
        var form_div = document.getElementById('order_form_wrap');
        for (var i = 0; i < 4; i++ )    // Remove last 4 nodes.
            form_div.removeChild(form_div.lastChild);
        
        // Create query string.
        var query_string = "sender_name={0}&sender_email={1}&message_text={2}".format(user_name, user_postal, user_text);

        // Make request.
        var req = new XMLHttpRequest();

        req.open('POST', '/send_email');
        req.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        req.send(query_string);     // Set Content-Length from string in send.

        req.onreadystatechange = function () {

            if(req.readyState === 4) {
                var res = req.responseText;

                var response_node = document.createElement('p');
                response_node.style['position'] = 'relative';
                response_node.style['top'] = '50%';
                response_node.style['textAlign'] = 'center';

                if (res === 'OK') {
                    var response_text = document.createTextNode('Спасибо, в ближайшее время мы с Вами свяжемся.');
                    response_node.appendChild(response_text);

                    form_div.appendChild(response_node);
                    
                } else {
                    var response_text = document.createTextNode('Не отправлено, где-то ошибка.');
                    response_node.appendChild(response_text);
                    form_div.appendChild(response_node);
                }
            } 

        }

        setTimeout(function() {
            toggleFrame("order_form_wrap")
        }, 4000);
    }
}


/* SEARCHING */
function hide_search_results() {
    setTimeout(function() {
        document.getElementById('search_result').style['visibility'] = 'hidden';
    }, 2000);
    //console.log('disable search results');
}
function search_content() {
    var content = document.getElementById('search_bar').childNodes[1].value;

    if(content.length) {
        //console.info('Try to search: ' + content);

        var search_req = new XMLHttpRequest();
        search_req.open('GET', '/quick_search?patt='+content);     
        search_req.send(null);

        search_req.onreadystatechange = function() {

            if(search_req.readyState === 4) {
                var data = JSON.parse(search_req.response);

                // If get not empty response.
                if (search_req.response.length > 2) // If nothing was found, it will return "{}"
                    document.getElementById('search_result').style['visibility'] = 'visible';
                    
                var result_list = document.getElementById('search_result').childNodes[1];

                // Delete all li.
                result_list.innerHTML = '';

                for (var i in data) {
                    //console.log( i + ' : ' + data[i] );

                    // Create link.
                    var new_link = document.createElement('a');
                    new_link.appendChild(document.createTextNode(data[i]));
                    // Set href of the link.
                    new_link.href = '/?Id=' + i;

                    // Create new list element.
                    var new_node = document.createElement('li');
                    new_node.appendChild(new_link);

                    // Add link to the list.
                    result_list.appendChild(new_node);
                }
            }

        }
    }
}


/* Comments. */
function add_comment(page_id) {

    if (!checkForm('add_comment')) {

        // Retrive user data.
        var form = document.getElementById('add_comment');

        // disable input button.
        form.getElementsByTagName('input')[2].setAttribute('disabled', 'disabled');

        var user_name = form.getElementsByTagName('input')[0].value;
        var user_email = form.getElementsByTagName('input')[1].value;
        var comment_text = form.getElementsByTagName('textarea')[0].value;

        // Create query string.
        var query_string = "text={0}&author_name={1}&author_email={2}&page_id={3}".format(comment_text, user_name, user_email, page_id);
        
        // Create ajax request.
        var req = new XMLHttpRequest();

        req.open('POST', '/create_comment');
        req.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        req.send(query_string);     // Set Content-Length from string in send.
        
        req.onreadystatechange = function() {
            if ( req.readyState == 4 ) {
                if (req.response == 'OK') {
                    location.reload();
                } else {
                    console.error(req.response);
                }
            }
        }
        
        //location.reload();
    }
}
