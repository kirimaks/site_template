jQuery(document).ready(function ($) {

    var _CaptionTransitions = [];
    /* Create captions effects. */
    _CaptionTransitions["transition_1"] = {$Duration:900,x:0.6,$Easing:{$Left:$JssorEasing$.$EaseInOutSine},$Opacity:2};
    _CaptionTransitions["transition_2"] = {$Duration:1200,x:0.6,y:0.6,$Zoom:3,$Rotate:0.3,$Easing:{$Left:$JssorEasing$.$EaseInCubic,$Top:$JssorEasing$.$EaseInCubic,$Rotate:$JssorEasing$.$EaseInElastic},$Opacity:2};
    _CaptionTransitions["transition_3"] = {$Duration:900,$Opacity:2};
    _CaptionTransitions["transition_4"] = {$Duration:900,y:-0.6,$Easing:{$Top:$JssorEasing$.$EaseInOutSine},$Opacity:2};

    /* Create slideshow effects. */
    var _SlideshowTransitions = [
        {$Duration:1200,$Opacity:2},
        {$Duration:1000,$Delay:80,$Cols:8,$Rows:4,$Opacity:2},
        {$Duration:1200,x:0.2,y:-0.1,$Delay:20,$Cols:8,$Rows:4,$Clip:15,$During:{$Left:[0.3,0.7],$Top:[0.3,0.7]},$SlideOut:true,$Formation:$JssorSlideshowFormations$.$FormationStraightStairs,$Assembly:260,$Easing:{$Left:$JssorEasing$.$EaseInWave,$Top:$JssorEasing$.$EaseInWave,$Clip:$JssorEasing$.$EaseOutQuad},$Outside:true,$Round:{$Left:1.3,$Top:2.5}},
        {$Duration:1800,x:1,y:0.2,$Delay:30,$Cols:10,$Rows:5,$Clip:15,$During:{$Left:[0.3,0.7],$Top:[0.3,0.7]},$SlideOut:true,$Reverse:true,$Formation:$JssorSlideshowFormations$.$FormationStraightStairs,$Assembly:2050,$Easing:{$Left:$JssorEasing$.$EaseInOutSine,$Top:$JssorEasing$.$EaseOutWave,$Clip:$JssorEasing$.$EaseInOutQuad},$Outside:true,$Round:{$Top:1.3}},
        {$Duration:1000,$Delay:30,$Cols:8,$Rows:4,$Clip:15,$Formation:$JssorSlideshowFormations$.$FormationStraightStairs,$Assembly:2050,$Easing:$JssorEasing$.$EaseInQuad}
    ];


    var options = { 
        $AutoPlay:true,

        $BulletNavigatorOptions: {
            $Class: $JssorBulletNavigator$,
            $ChanceToShow: 2
        },

        $ArrowNavigatorOptions: {
            $Class: $JssorArrowNavigator$,
            $ChanceToShow: 2
        },

        $CaptionSliderOptions: {
            $Class: $JssorCaptionSlider$,
            $CaptionTransitions: _CaptionTransitions,
            $PlayInMode: 1,
            $PlayOutMode: 3
        },

        $SlideshowOptions: {
            $Class: $JssorSlideshowRunner$,
            $Transitions: _SlideshowTransitions,
            $TransitionsOrder: 1,
            $ShowLink: true
        }

    };

    var jssor_slider1 = new $JssorSlider$('slider1_container', options);
});
