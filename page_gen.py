#!/usr/bin/python
#-*- coding: utf-8 -*-

# This module generates appropriate page.

import re
import tools
import codecs
import os.path
from string import Template
from collections import defaultdict

class PageGen(object):
    
    def __init__(self, __db_tool__):
        # Paths.
        # Paths. TODO: make a "path" module.
        self.__base_path = u'/home/k/kirimaksya/kirimaksya.tmp/'
        self.__template_path = os.path.join(self.__base_path, 'templates')

        # Pages paths.
        self.pages_paths = {    # TODO: think about this name.
            # Different pages.
            u'/'              : self.home_page,
            u'/home'          : self.home_page,
            u'/prices'        : self.prices_page,
            u'/hotels.html'   : self.hotels_iframe,

            # Thumbnails. 
            u'/ekskursii-v-sochi'             : lambda req : self.thumbnails('excursions', req),
            u'/otdyh-v-sochi'                 : lambda req : self.thumbnails('recreation', req),
            u'/dostoprimecatelnosti-v-sochi'  : lambda req : self.thumbnails('sights', req),
            u'/oteli-v-sochi'                 : lambda req : self.thumbnails('hotels', req),

            #------------ Articles. -------------------
            # Excursions.
            u'/33-vodopada'           : lambda req : self.an_article(7),
            u'/volkonskij-dolmen'     : lambda req : self.an_article(8),
            u'/agurskie-vodopady'     : lambda req : self.an_article(9),
            u'/gora-ahun'             : lambda req : self.an_article(10),
            u'/dacha-stalina'         : lambda req : self.an_article(11),

            u'/vecernij-ahun'         : lambda req : self.an_article(12),
            u'/voroncovskie-pesery'   : lambda req : self.an_article(13),
            u'/zmejkovskie-vodopady'  : lambda req : self.an_article(14),
            u'/obezjannij-pitomnik'   : lambda req : self.an_article(15),
            u'/zenskij-monastyr'      : lambda req : self.an_article(16),
            u'/adlerskij-okeanarium'  : lambda req : self.an_article(17),
            u'/adlerskij-delfinarij'  : lambda req : self.an_article(18),
            u'/tiso-samsitovaja-rosa' : lambda req : self.an_article(19),
            u'/cajnye-domiki'         : lambda req : self.an_article(20),
            u'/olimpijskij-park'      : lambda req : self.an_article(21),
            u'/ozera-ljubvi'          : lambda req : self.an_article(22),
            u'/derevo-druzby'         : lambda req : self.an_article(23),
            u'/park-sochi'            : lambda req : self.an_article(24),
            u'/v-gostjah-u-pasecnika' : lambda req : self.an_article(25),
            u'/orehovskie-vodopady'   : lambda req : self.an_article(26),
            u'/soloh-aul'             : lambda req : self.an_article(27),

            u'/poselok-krasnaja-poljana'  : lambda req : self.an_article(28),
            u'/forelevoe-hozjajstvo'      : lambda req : self.an_article(29),
            u'/splav-po-mzymte'           : lambda req : self.an_article(30),
            u'/sou-stekloduvov'           : lambda req : self.an_article(31),
            u'/roza-hutor'                : lambda req : self.an_article(32),
            u'/gorod-gorki'               : lambda req : self.an_article(33),
            u'/gornaja-karusel'           : lambda req : self.an_article(34),
            u'/vodopad-stany'             : lambda req : self.an_article(35),
            u'/vodopad-devici-slezy'      : lambda req : self.an_article(36),

            u'/zero-rica'            : lambda req : self.an_article(37),
            u'/novoafonskij-monastyr' : lambda req : self.an_article(38),
            u'/novoafonskaja-pesera'  : lambda req : self.an_article(39),
            u'/gagra'                 : lambda req : self.an_article(40),
            u'/picunda'               : lambda req : self.an_article(41),
            u'/suhum'                 : lambda req : self.an_article(42),
            u'/hram-simona-kananita'  : lambda req : self.an_article(43),
            u'/po-svjatym-mestam'     : lambda req : self.an_article(44),

            # Recreation.
            u'/dendrarij'         : lambda req : self.an_article(50),
            u'/park-rivera'       : lambda req : self.an_article(51),
            u'/festivalnyj'       : lambda req : self.an_article(52),
            u'/akvapark-majak'    : lambda req : self.an_article(53),
            u'/circus'            : lambda req : self.an_article(54),
            u'/sinee-more'        : lambda req : self.an_article(55),
            u'/plotforma'         : lambda req : self.an_article(56),
            
            u'/sajba'             : lambda req : self.an_article(57),
            u'/bolsoj'            : lambda req : self.an_article(58),
            u'/iceberg'           : lambda req : self.an_article(59),
            u'/adler-arena'       : lambda req : self.an_article(60),
            u'/akvapark-amfibius' : lambda req : self.an_article(61),
            u'/strausinaja-ferma' : lambda req : self.an_article(62),
            u'/ledjanoj-kub'      : lambda req : self.an_article(63),
            u'/adlerskij-park'    : lambda req : self.an_article(64),

            u'/serebrjannoe-ozero'    : lambda req : self.an_article(65),
            u'/promitey'              : lambda req : self.an_article(66),
            u'/certovy-vorota'        : lambda req : self.an_article(67),
            u'/navalisenskoe-usele'   : lambda req : self.an_article(68),

            u'/volernyj-kompleks'     : lambda req : self.an_article(69),
            u'/hmelevskie-ozera'      : lambda req : self.an_article(70),
            u'/ahstyrskij-kanon'      : lambda req : self.an_article(71),

            u'/akva-loo'          : lambda req : self.an_article(72),
            u'/banja-v-dagomyse'  : lambda req : self.an_article(73),

            # Sights.
            u'/morport'                   : lambda req : self.an_article(80),
            u'/primorskaja-nabereznaja'   : lambda req : self.an_article(81),
            u'/socinskij-majak'           : lambda req : self.an_article(82),
            u'/zimnij-teatr'              : lambda req : self.an_article(83),
            u'/pamjatnik-jakor-i-puska'   : lambda req : self.an_article(84),
            u'/muzej-istorii-sochi'       : lambda req : self.an_article(85),
            u'/biblioteka-imeni-puskina'  : lambda req : self.an_article(86),
            u'/letnij-teatr'              : lambda req : self.an_article(87),
            u'/hudozestvennyj-muzej'      : lambda req : self.an_article(88),
            u'/zeleznodoroznyj-vokzal'    : lambda req : self.an_article(89),
            u'/skulptury-iz-filma-brilliantovaja-ruka'    : lambda req : self.an_article(90),
            u'/cerkov-imeni-svjatogo-vladimira'           : lambda req : self.an_article(91),
            u'/glavpoctampt'                              : lambda req : self.an_article(92),

            u'/mezdunarodnyj-aeroport'            : lambda req : self.an_article(93),
            u'/adlerskij-zeleznodoroznyj-vokzal'  : lambda req : self.an_article(94),
            u'/adlerskij-majak'                   : lambda req : self.an_article(95),

            u'/macestinskij-most'     : lambda req : self.an_article(96),
            u'/macesta'               : lambda req : self.an_article(97),
            u'/pamjatnik-maceste'     : lambda req : self.an_article(98),
            u'/macestinskij-istocnik' : lambda req : self.an_article(99),

            u'/tjulpanovoe-derevo'    : lambda req : self.an_article(100),
            u'/usadba-kosmana'        : lambda req : self.an_article(101),
            u'/kompleks-dagomys'      : lambda req : self.an_article(102),

            #------------------------------------------
        }

        # Create mysql connection.
        self.__db_tool = __db_tool__

        # Templates with video.
        self.__video_pages = {
        # Article Id | Path to template with video.
            u'8' : os.path.join( self.__template_path,  u'article/video/article_8_video.html' ),
            u'10' : os.path.join( self.__template_path,  u'article/video/article_10_video.html' ),
        }


    @property
    def template_path(self):
        return self.__template_path


    def __call__( self, req ):
        # Generate page by Id (the path should be '/'). 
        if req.path_info == u'/' and (u'Id' in req.GET.keys()):
            return self.an_article(req.GET['Id'])
            
        return self.pages_paths[req.path_info](None)


    #--------------- HOME PAGE ---------------------------------
    def home_page(self, req):
        xml_tool = tools.Xml_tool()

        #------ GET WEATHER DATA -------
        cur_temp = xml_tool.get_cur_temp()
        cur_weather_cond = xml_tool.get_cur_weather_condition()
        cur_water_temp = xml_tool.get_water_temp()
        
        #-------------------------------

        page = []

        # Page header(CSS, javascript).
        with open(os.path.join(self.template_path, 'home/home_page_header.html')) as header:
            page.append(header.read().decode(u'utf-8'))

        ##### GENERATE BODY CONTENT ####

        # Add loader.
        page.append(u"<div id='loader'></div>")


        # Statistic page and open "main" div.
        with open(os.path.join(self.template_path, 'statistic.html')) as stat:
            page.append(stat.read().decode(u'utf-8'))

        # Default (static) body content.
        with open( os.path.join( self.template_path, 'home/home_page_top.html')) as top_part:
            # Weather template.
            template = Template(top_part.read().decode(u'utf-8'))
            page.append(template.substitute( dict(                      \
                                weather_temp = cur_temp,            \
                                weather_cond = cur_weather_cond,    \
                                water_temp = cur_water_temp ) ) ) 

        # Fixed buttons.
        with open(os.path.join(self.template_path, 'fixed_content.html')) as buttons:
            page.append( buttons.read().decode(u'utf-8'))


        # Dynamic body content.
        with open(os.path.join(self.template_path, 'home/home_page_body.html')) as body:
            page.append( body.read().decode(u'utf-8') )

        ### BODY END ###

        # Page footer.
        with open(os.path.join(self.template_path, 'excursions_footer.html')) as footer:
            page.append( footer.read().decode(u'utf-8') )

        return page
    #-----------------------------------------------------------


    #----------------- ARTICLE ---------------------------------
    def an_article(self, Id):

        page = []

        # Trying to get data and make exceptin if wrong Id was submitted.
        try:
            article_title = self.__db_tool.get_article_title(Id)
            article_full_text = self.__db_tool.get_article_text(Id)
            article_name = self.__db_tool.get_article_name(Id)
            article_type = self.__db_tool.get_article_type(Id)

        except (TypeError, tools.MySQLdb.OperationalError):
            return None # Make 301 redirect.
        


        # Page header(CSS).
        with open(os.path.join(self.template_path, 'article/article_page_header.html')) as header:
            template = Template(header.read().decode(u'utf-8'))
            page.append( template.substitute( dict( title=article_title )) )


        # Statistic page and open "main" div.
        with open(os.path.join(self.template_path, 'statistic.html')) as stat:
            page.append( stat.read().decode(u'utf-8') )
        

        # Default (static) body content.
        with open(os.path.join(self.template_path, 'default_body_with_menu.html')) as body:
            page.append( body.read().decode(u'utf-8') )


        # Fixed buttons.
        with open(os.path.join(self.template_path, 'fixed_content.html')) as buttons:
            page.append( buttons.read().decode(u'utf-8') )



        ####### Article left menu. #####
        # Choise article type and generate appropriate menu template.
        cur_dict = defaultdict(lambda : u'')
        article_template_types = {
            u'excursion' : u'article/excursions_left_menu.html',
            u'recreation': u'article/recreation_left_menu.html',
            u'sights'    : u'article/sights_left_menu.html'
        }
        with open(os.path.join(self.template_path, article_template_types[article_type] )) as menu:
                cur_dict.update( { article_name : u'current_article' } )
                page.append( menu.read().decode(u'utf-8') % cur_dict )


        ########## Create top part. ########
        with open(os.path.join(self.template_path, 'article/article_body_top.html')) as top_part:
            page.append( top_part.read().decode(u'utf-8') % (article_title, article_full_text) )



        #### ADD PROMO BAMER ####
        with open(os.path.join(self.template_path, 'promo/contema.html')) as body_baner:
            page.append( body_baner.read().decode(u'utf-8') )


        ###### ADD VIDEO if exists. ######
        if unicode(Id) in self.__video_pages.keys():
            with open(self.__video_pages[unicode(Id)]) as video_content:
                page.append( video_content.read().decode(u'utf-8') )
        ##################################


        ##### GENERATE PICTURES ####
        # CODE: Search any files that contain jpg or JPG.

        # Prepath of pics (just don't want to write if).
        # TODO: maybe place it in top.
        article_types = {
            u'excursion'    : u'media/pics/excursions',
            u'recreation'   : u'media/pics/rest',
            u'sights'       : u'media/pics/sights',
        }
        # Path for web server.
        prepath = os.path.join(article_types[article_type], article_name)

        reObj = re.compile(u'jpg$|JPG$')
        pics_paths = []

        cur_path = os.path.join(self.__base_path, prepath)

        # List of object in directory and if mach then add to pics array.
        for root, dirs, files in os.walk(str(cur_path)):
            for file_name in files:
                if reObj.search(file_name):
                    pics_paths.append(os.path.join(prepath, unicode(file_name, 'utf-8')))

        #####
        page.append(u'''
                <!-- PICS BEGIN -->
                <div id='photo_gallery'>
        ''')

        for path in pics_paths:
            path_to_preview = os.path.dirname(path) + u'/previews/' + os.path.basename(path)
            page.append(u'''
                <a class='photo_gallery' href='%s'>
                    <img src='%s' alt='%s'/>
                </a>
            ''' % (path, path_to_preview, article_title ) )

        page.append(u'''
                </div>
                <!-- PICS END -->
        ''')
        #------------------------------------------


        ########### COMMENTS ############
        comment_number = self.__db_tool.article_comments_number(Id)

        # If there is 0 comment for this article, then print nothing.
        if comment_number:
            page.append(u'<h2 class="comments_header">Комментарии:</h2>')

            #comment_template = Template('''
            #<div id="article_comment">
            #    <span>${author}</span>
            #    <span>${date}</span>
            #    <hr/>
            #    <p>${text}</p>
            #</div>
            #''')

            cf = open(os.path.join(self.template_path, 'article/comment.html'))
            comment_template = Template(''.join(cf.readlines()))
            cf.close()

            comments = self.__db_tool.get_comments(Id)

            for comment in comments:
                page.append(comment_template.substitute(dict(author=comment[1], date=comment[4], text=comment[0])))
        #######################################

        ### ADD Comment form ###
        form_file = codecs.open(os.path.join(self.template_path, 'article/add_comment.html'), encoding='utf-8')
        form_template = Template(u''.join(form_file.readlines()))
        form_file.close()
        page.append(form_template.substitute(dict(page_id=Id)))

        

        # TODO: move this code to templates.
        ### ADD Comment form ###
        #page.append(u'<h2 class="comments_header">Оставить комментарий:</h2>')
        #page.append(u'''
        #<form id="add_comment" method="POST">
        #    <label>Ваше имя:</label>
        #    <input type="text" name="author_name" required />
        #    <br/>

        #    <label>Ваша почта:</label>
        #    <input type="email" name="author_postal" required />
        #    <br/>

        #    <label>Текст сообщения:</label>
        #    <textarea rows="10" cols="68" name="comment_text"></textarea>
        #    <input type="button" onclick="add_comment(%s)" value="Отправить" />
        #</form>
        #''' % (Id,) )
        ########################

        
        page.append(u'</div></section></div>')

        ###### Page footer #####.
        # TODO: Choice page type and generate appropriate footer.
        #
        with open(os.path.join(self.template_path, 'excursions_footer.html')) as footer:
            page.append( footer.read().decode(u'utf-8') )

        return page
    #-----------------------------------------------------------


    def prices_page(self, req):
        page = []

        # Page header(CSS).
        with open(os.path.join(self.template_path, 'prices_header.html')) as header:
            page.append(header.read().decode(u'utf-8'))

        # Statistic page and open "main" div.
        with open(os.path.join(self.template_path, 'statistic.html')) as stat:
            page.append(stat.read().decode(u'utf-8'))

        # Default (static) body content.
        with open(os.path.join(self.template_path, 'default_body_with_menu.html')) as body:
            page.append(body.read().decode(u'utf-8'))

        # Fixed buttons.
        with open(os.path.join(self.template_path, 'fixed_content.html')) as buttons:
            page.append(buttons.read().decode(u'utf-8'))

        with open(os.path.join(self.template_path, 'article/excursions_left_menu.html')) as menu:
            page.append( menu.read().decode(u'utf-8'))

        with open(os.path.join(self.template_path, 'prices_body.html')) as body:
            page.append(body.read().decode(u'utf-8'))

        #### FOOTER ####
        with open(os.path.join(self.template_path, 'excursions_footer.html')) as footer:
            page.append(footer.read().decode(u'utf-8'))

        return page


    def hotels_iframe(self, req):
        page = ''

        with open(os.path.join(self.__base_path, 'var/hotels.html')) as hotels:
            page = hotels.read()

        return page


    def thumbnails(self, page_type, req):
        page = []

        ### NEW Thumbnails template. TODO: add footers template here. #####
        thumbnail_types = {
            # Type | Title, template.
            u'excursions' : [ u'Экскурсии в Сочи', u'excursions_tn_body.html', u'excursions_footer.html' ],
            u'recreation' : [ u'Отдых в Сочи', u'recreation_tn_body.html', u'excursions_footer.html'],
            u'sights'     : [ u'Достопримечательности в Сочи', u'sights_tn_body.html', u'excursions_footer.html' ],
            u'hotels'     : [ u'Гостиницы и отели в Сочи', u'hotels_tn_body.html', u'excursions_footer.html' ],
        }

        # Page header(CSS). --------------------------------
        with open(os.path.join(self.template_path, u'thumbnails_header.html')) as header:
            page.append( header.read().decode(u'utf-8') % (thumbnail_types[page_type][0]) )

        ##### GENERATE BODY CONTENT ####
        # Add loader.
        page.append( u"<div id='loader'></div>" )

        # Statistic page and open "main" div.
        with open(os.path.join(self.template_path, 'statistic.html')) as stat:
            page.append( stat.read().decode(u'utf-8') )

        # Default (static) body content.
        with open(os.path.join(self.template_path, 'default_body_with_menu.html')) as body:
            page.append( body.read().decode(u'utf-8') )


        # Fixed buttons.
        with open(os.path.join(self.template_path, 'fixed_content.html')) as buttons:
            page.append( buttons.read().decode(u'utf-8') )


        # CREATE BODY and FOOTER.
        with open(os.path.join(self.template_path, thumbnail_types[page_type][1])) as body:
            page.append( body.read().decode(u'utf-8') )
        with open(os.path.join(self.template_path, thumbnail_types[page_type][2])) as footer:
            page.append( footer.read().decode(u'utf-8') )

        return page
    #-----------------------------------------------------------

