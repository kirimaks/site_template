#!/usr/bin/env python2 
# -*- coding: utf-8 -*-

import os.path
import site
site.addsitedir(os.path.split(__file__)[0])

import webob
import webob.exc
import page_gen
import site_services
import tools

db_tool = tools.Db_tool()

# Create objects and pass db_tool to it.
page_generator = page_gen.PageGen(db_tool)
services_handler = site_services.SiteServices(db_tool)

def application(environ, start_response):

    # Create WebOB request.
    req = webob.Request(environ)

    page = None

    # Based on the request generate page or run a service.
    if req.path_info in page_generator.pages_paths.keys():
        page = page_generator( req )

    elif req.path_info in services_handler.known_services.keys():
        page = services_handler( req )

    # Make 301 redirect if page is empty (can happen if wrong Id).
    if page == None:
        exc = webob.exc.HTTPTemporaryRedirect(location = req.host_url + u'/home')
        return exc(environ, start_response)

    # Create WebOB response.
    res = webob.Response()
    res.charset = 'utf-8'
    
    # Headers.
    res.headers['Content-Type'] = 'text/html; charset=utf-8'
    #res.headers.add('Path-Info', str(req.path_info)) # Debug.
    #res.headers.add('Host-Name', str(req.host_url)) # Debug.
    #res.headers.add('Page-Type', str(type(page))) # Debug.

    # page should be iterable here.
    body_iter = res.body_file
    for line in page:
        body_iter.write(line)

    return res(environ, start_response)

