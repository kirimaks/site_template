#!/usr/bin/env python2
# -*- coding: utf-8 -*-

import MySQLdb
import codecs
from xml.dom import minidom
import smtplib
import time

###
import ConfigParser
config = ConfigParser.RawConfigParser()
config.read(u'/home/k/kirimaksya/kirimaksya.tmp/config.cfg')
###

class Db_tool(object):

    def __init__(self):
        pass

    def connect_db(self):
        self.__db = MySQLdb.connect( 
            host = config.get('Database', 'host'), 
            user = config.get('Database', 'user'), 
            passwd = config.get('Database', 'pass'),
            db = config.get('Database', 'db_name'), 
            charset='utf8', use_unicode=True 
        )

        self.__curr = self.__db.cursor()

    def close_db(self):
        self.__db.commit()
        self.__db.close()

    def get_id_list(self):
        id_list = []

        self.connect_db()
        self.__curr.execute('SELECT Id FROM Pages')

        for row in self.__curr.fetchall():
            id_list.append(row[0])

        self.close_db()

        return id_list

    def get_article_title(self, Id):
        self.connect_db()
        self.__curr.execute('SELECT PageTitle FROM Pages WHERE Id = %s' % (Id,))
        title = self.__curr.fetchone()
        self.close_db()
        return title[0]

    def get_article_name(self, Id):
        self.connect_db()
        self.__curr.execute('SELECT PageName FROM Pages WHERE Id = %s' % (Id,))
        name = self.__curr.fetchone()
        self.close_db()
        return name[0]

    def get_article_type(self, Id):
        self.connect_db()
        self.__curr.execute('SELECT PageType FROM Pages WHERE Id = %s' % (Id,))
        page_type = self.__curr.fetchone()
        self.close_db()
        return page_type[0]




    def get_article_top_image(self, Id):
        self.connect_db()
        self.__curr.execute('SELECT PicPath FROM PicsPaths WHERE ImgType = "top" AND PageId = %s' % (Id,))
        image_path = self.__curr.fetchone()
        self.close_db()

        if image_path != None:
            return image_path[0]
        else : return []

    def get_article_text(self, Id):
        self.connect_db()
        self.__curr.execute('SELECT Record FROM Records WHERE PageId = %s' % (Id,))
        full_text = self.__curr.fetchone()
        self.close_db()
        return full_text[0]

    def get_article_gallery_pics_paths(self, Id):
        pics_paths = []

        self.connect_db()
        self.__curr.execute('SELECT PicPath FROM PicsPaths WHERE PageId=%s AND ImgType = "gallery"' % (Id,))

        for path in self.__curr.fetchall():
            pics_paths.append(path[0])

        self.close_db()
        return pics_paths


    def delete_from_table( self, tab_name, what, Id ):
        self.connect_db()
        self.__curr.execute('DELETE FROM %s WHERE %s = %s' % ( tab_name, what, Id ))
        self.close_db()


    def insert_into( self, tab_name, cols, vals ):
        cmd = u"INSERT INTO " + tab_name+u"( %s ) VALUES( '%s' )" % \
            (",".join(cols), "','".join(vals))
        
        with codecs.open(u'/tmp/debug.txt', u'w+', encoding=u'utf-8') as out:
            out.write(cmd)

        self.connect_db()
        self.__curr.execute(cmd)
        self.close_db()

    ###################
    #     Search.     #
    ###################
    def long_search(self, patt):
        self.connect_db()
        self.__curr.execute('SELECT Id,PageTitle FROM Pages WHERE PageTitle LIKE "%%%s%%"' % (patt,))
        data = self.__curr.fetchall()
        self.close_db()
        return data

    def spelling_search(self, patt):
        self.connect_db()
        self.__curr.execute('SELECT Id,PageTitle FROM Pages WHERE PageTitle LIKE "%s%%"' % (patt,))
        data = self.__curr.fetchall()
        self.close_db()
        return data


    ##############
    ## COMMENTS ##
    ##############
    def article_comments_number(self, Id): 
        self.connect_db()
        self.__curr.execute('SELECT count(*) FROM Comments WHERE PageId = %s' % (Id,))
        num = self.__curr.fetchall()
        self.close_db()
        return num[0][0]

    def get_comments(self, Id):
        self.connect_db()
        self.__curr.execute('SELECT Record, AuthorName, AuthorEmail, published, Date FROM Comments WHERE PageId = %s' % (Id,))
        data = self.__curr.fetchall()
        self.close_db()
        return data

    def create_comment(self, text, author_name, author_email, page_id ):
        self.connect_db()
        self.__curr.execute('INSERT INTO Comments(Record, AuthorName, AuthorEmail, published, PageId, Date) \
                                VALUES("%s", "%s", "%s", 1, %s, "%s")' % \
                                (text, author_name, author_email, page_id, time.strftime("%d-%m-%y") ) ) 
        self.close_db()
 
        

class Xml_tool(object):

    def __init__(self):
        self.__cur_temp = u''
        self.__cur_weather_cond = u''
        self.__cur_water_temp = u''

        file_path = u'/home/k/kirimaksya/kirimaksya.tmp/var/ya_weather.xml'
    
        root = minidom.parse(file_path).firstChild

        self.__cur_temp = root.getElementsByTagName(u'temperature')[0].firstChild.data
        self.__cur_weather_cond = root.getElementsByTagName(u'weather_type')[0].firstChild.data
        self.__cur_water_temp = root.getElementsByTagName(u'water_temperature')[0].firstChild.data

        # Get the sing of weather and add to cur_weather.
        self.__the_sign = root.getElementsByTagName(u'image-v2')[0].firstChild.data
        if u'+' in self.__the_sign:
            self.__cur_temp = u'+' + self.__cur_temp
        else:
            self.__cur_temp = u'-' + self.__cur_temp
            

    def get_cur_temp(self):
        return self.__cur_temp

    def get_cur_weather_condition(self):
        return self.__cur_weather_cond
            
    def get_water_temp(self):
        return self.__cur_water_temp

       

