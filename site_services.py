#!/usr/bin/env python2
#-*- coding: utf-8 -*-

import os.path
import site
site.addsitedir(os.path.split(__file__)[0])

from string import Template
import smtplib
import json
import codecs
from string import Template

import ConfigParser
config = ConfigParser.RawConfigParser()
config.read(u'/home/k/kirimaksya/kirimaksya.tmp/config.cfg')

class SiteServices(object):
    
    def __init__(self, db_tool):
        # Paths. TODO: make a "path" module.
        self.__base_path = u'/home/k/kirimaksya/kirimaksya.tmp/'
        self.__template_path = os.path.join(self.__base_path, 'templates')

        self.__db_tool = db_tool

        self.known_services = {
            u'/send_email' : self.send_mail,
            u'/quick_search' : self.quick_search,
            u'/full_search' : self.full_search,
            u'/create_comment' : self.create_comment,
        }

        # Mail settings.
        self.__smtp_server = config.get('Mail', 'server')
        self.__server_user = config.get('Mail', 'user')
        self.__server_pass = config.get('Mail', 'pass')

    def __call__(self, req):
        return self.known_services[req.path_info](req)

    ################
    #   Comments   #
    ################
    def create_comment(self, req):
        page = []

        try:
            text = req.POST['text']
            author_name = req.POST['author_name']
            author_email = req.POST['author_email']
            page_id = req.POST['page_id']

            self.__db_tool.create_comment(text, author_name, author_email, page_id )
        
            page.append(u'OK')

            from email.mime.text import MIMEText

            sender = 'admin@grand-sochi.ru'
            me = 'kirimaks@yahoo.com'
            
            msg = MIMEText('A comment for Id:[%s] is created' % page_id )
            msg['Subject'] = 'Comment notification.'
            msg['From'] = sender
            msg['To'] = me


            smtpObj = smtplib.SMTP(self.__smtp_server)
            smtpObj.ehlo()
            smtpObj.starttls()
            smtpObj.login( self.__server_user, self.__server_pass)
            smtpObj.sendmail(sender, [me], msg.as_string())


        except Exception as Exc:
            page.append(Exc.message)
            page.append(u'ERR')
    
        return page
        


    ####################
    #     Search       #
    ####################
    def quick_search(self, req):
        page = []

        data_buff = {}

        try:
            pt_str = req.GET['patt']
        except KeyError:
            return None

        if len(pt_str) >= 4:
            data = self.__db_tool.long_search(pt_str)
        else:
            data = self.__db_tool.spelling_search(pt_str)
        
        for ln in data:
            data_buff[ln[0]] = ln[1]

        json_data = json.dumps(data_buff, indent=2, separators=(u',', u'\t:\t') )
        page.append(json_data)

        return page

    # Return search results page.
    def full_search(self, req):
        page = []

        try:
            pattern = req.GET['patt']
        except KeyError:
            return None     # Make 301 redirect to home page.

        with codecs.open(self.__template_path + '/search/search_header.html') as head:
            template = Template(head.read().decode('utf-8'))
            page.append(template.substitute(dict(title=u'Результаты поиска')))

        with codecs.open(self.__template_path + '/statistic.html') as stat:
            page.append(stat.read().decode('utf-8'))

        with codecs.open(self.__template_path + '/default_body_with_menu.html') as body:
            page.append(body.read().decode('utf-8'))

        with codecs.open(self.__template_path + '/fixed_content.html') as body:
            page.append(body.read().decode('utf-8'))

        with codecs.open(self.__template_path + '/search/search_results.html') as body:
            page.append(body.read().decode('utf-8'))

        page.append('<ul>')
        
        data = self.__db_tool.long_search(pattern)

        if len(pattern) == 0 or len(data) == 0:
            page.append('<li class="search_field"><a href="/home">Ничего не найдено</a></li>\n')

        else:
            for ln in data:
                #data_buff[ln[0]] = ln[1]
                page.append('<li class="search_field"><a href="/?Id=%s">%s</a></li>\n' % (ln[0], ln[1]))

        page.append('</ul>')
        
        page.append('</div></section></div> <!--main-->')

        with codecs.open(self.__template_path + '/excursions_footer.html') as footer:
            page.append(footer.read().decode('utf-8'))


        #page.append('<h1>Search results for [%s]</h1>' % (pattern,))

        return page


    ############
    #   Mail   #
    ############
    def send_mail(self, req):

        page = []

        try:
            sender = u'admin@grand-sochi.ru'
            receivers = [ u'kirimaks@yahoo.com', u'kirimakz@gmail.com', u'arinmaks@mail.ru' ]

            messageTemplate = Template(u"""From: ${sender_name} <${sender_email}>
To: ${receiver_name} <${receiver_email}>
MIME-Version: 1.0
Content-type: text/html; charset=utf-8
Subject: ${message_subject}

----------------------------------------------------------------------
</br>
${message_text}
</br>
----------------------------------------------------------------------
""")

            dict_data = {
                    u'sender_name' : req.POST[u'sender_name'],
                    u'sender_email' : req.POST[u'sender_email'],
                    u'receiver_name' : u'Ирина',
                    u'receiver_email' : u'arinmaks@mail.ru',
                    u'message_subject' : u'Grand-Sochi Вопрос',
                    u'message_text' : req.POST[u'message_text'],
            }

            message = messageTemplate.substitute(dict_data)


            # Define server data.  
            #self.__smtp_server = config.get('Mail', 'server')
            #self.__server_user = config.get('Mail', 'user')
            #self.__server_pass = config.get('Mail', 'pass')

            smtpObj = smtplib.SMTP(self.__smtp_server)

            smtpObj.ehlo()
            smtpObj.starttls()

            smtpObj.login( self.__server_user, self.__server_pass)
            smtpObj.sendmail(sender, receivers, message.encode('utf-8'))


            page.append('OK')

        except Exception as Exc:
            page.append(str(Exc))
            page.append(Exc.message)
            page.append('\nERR\n')

        return page
