#!/usr/bin/env python2
#-*- coding: utf-8 -*-

import urllib
import codecs
from xml.dom import minidom
import os.path

yandex_sochi_weather = u'http://export.yandex.ru/weather-ng/forecasts/37099.xml'
root = minidom.parse(urllib.urlopen(yandex_sochi_weather)).firstChild.childNodes[1]

out_file_name = 'ya_weather.xml'
out_file_path = '/home/k/kirimaksya/kirimaksya.tmp/var/'

with codecs.open(os.path.join(out_file_path, out_file_name), "w", encoding="utf-8") as out:
    out.write('<?xml version="1.0" encoding="utf-8" ?>\n')
    root.writexml(out, indent="", addindent="", newl="")
