#!/usr/bin/env python2
#-*- coding: utf-8 -*-

import os.path
import codecs
import urllib
from HTMLParser import HTMLParser 


class Get_section(HTMLParser):

    def my_init(self, file_name):
        self.output = codecs.open(file_name, 'w', encoding='utf-8')
        self.my_section = False

        self.self_closed = ['meta', 'link']

        ### LINKS ###
        self.last_link = 'None'
        self.base_hotel_link = 'http://booking.com/hotel/ru/'


    def handle_starttag(self, tag, attrs):

        if tag == 'div':
            for cur_att in attrs:

                # Start of section.
                if cur_att[0] == 'class' and cur_att[1] == 'hotellist':
                    self.my_section = True      

                # End of section.
                elif cur_att[0] == 'class' and cur_att[1] == 'results-meta' and self.my_section:
                    self.my_section = False


        # Write to file if in seciton.
        if self.my_section:
            self.output.write(u'<'+tag)

            if len(attrs):
                self.output.write(u' ')

            # Write attributes.
            for cur_attr in attrs:
                param = cur_attr[0]
                arg = u'None' if cur_attr[1] == None else cur_attr[1]


                ### TEST (about links) #############################
                if tag == 'a' and param == 'href':
                    # Exclude some params.

                    if arg != 'javascript:void(0)':

                        if arg == '#':
                            arg = self.base_hotel_link + os.path.split(self.last_link)[-1]

                        else:
                            arg = 'http://booking.com' + arg + '?aid=835708'
                            self.last_link = arg

                        #print arg
                ####################################################


                self.output.write(param + u'=' + u"'" + arg + u"' ")

            self.output.write(u'>')


    def handle_endtag(self, tag):
        #print u"Encountered end tag : ", tag
        if self.my_section:
            self.output.write('</' + tag + '>')
        if tag == 'section' and self.my_section:
            self.my_section = False
            self.output.write('\n')


    def handle_data(self, data):
        #print u"Encountered some data : ", data
        if self.my_section:
            self.output.write(data)



class Get_header(Get_section):

    def handle_starttag(self, tag, attrs):

        # Start from head.
        #if tag == 'head':
        #    self.my_section = True      


        # Write to file if in seciton.
        if self.my_section:
            self.output.write(u'<'+tag)

            if len(attrs):
                self.output.write(u' ')

            # Write attributes.
            for cur_attr in attrs:
                self.output.write(cur_attr[0] + '=' + "'" + cur_attr[1] + "' ")

            if tag in self.self_closed:
                self.output.write(u'/>')
            else:
                self.output.write(u'>')

        # Start from head.
        if tag == 'head':
            self.my_section = True      


    def handle_endtag(self, tag):
        #print u"Encountered end tag : ", tag

        #if self.my_section and (tag not in self.self_closed):
        #    self.output.write('</' + tag + '>')

        if tag == 'head' and self.my_section:
            self.my_section = False
            self.output.write('\n')

        if self.my_section and (tag not in self.self_closed):
            self.output.write('</' + tag + '>')



site_url = "http://www.booking.com/searchresults.ru.html?aid=835708;sid=a0ed368be5b7683dbee557044086a4ac;dcid=1;city=-3006514"


# Get section.
parser = Get_section()
parser.my_init('/tmp/section.html')
parser.feed(urllib.urlopen(site_url).read().decode('utf-8'))

# Get header.
parser = Get_header()
parser.my_init('/tmp/head.html')
parser.feed(urllib.urlopen(site_url).read().decode('utf-8'))

# Create document.
with codecs.open('/home/k/kirimaksya/kirimaksya.tmp/var/hotels.html', 'w', encoding='utf-8') as doc:
    doc.write('<!doctype html>\n<html>\n\n')

    doc.write('<head>\n\n')

    # Create header.
    for line in codecs.open('/tmp/head.html', encoding='utf-8'):
        doc.write(line)

    doc.write('<style type="text/css"> #v-promo-sr{display:none;} body{background:none;} </style>')

    doc.write('\n\n</head>\n\n')
        
    # Create body.
    doc.write('\n<body>\n')
    for line in codecs.open('/tmp/section.html', encoding='utf-8'):
        doc.write(line)
    doc.write('\n</body>\n')

    # End.
    doc.write('\n</html>\n')
